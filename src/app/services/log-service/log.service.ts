import {Observable} from 'rxjs';
import {Log} from '../../models/log';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {saveAs} from 'file-saver';
import {TokenStorage} from '../../core/token.storage';

const MILLISECONDS_PER_DAY = 86400000;

export class LogService {
  private _logs: Log[];

  private _markedLogId: number;
  private _currentSource: string;

  private _currentPage: number = 1;
  private _pageSize: number = 20;
  private _totalItems: number;

  private _dateStart: number;
  private _dateEnd: number;

  get dateStart(): number {
    return this._dateStart;
  }

  set dateStart(value: number) {
    this._dateStart = value;
  }

  get dateEnd(): number {
    return this._dateEnd;
  }

  set dateEnd(value: number) {
    this._dateEnd = value;
  }

  get currentSource(): string {
    return this._currentSource;
  }

  set currentSource(value: string) {
    this._currentSource = value;
    this.getTotalItems().subscribe(num => this.totalItems = num);
  }

  get currentPage(): number {
    return this._currentPage;
  }

  set currentPage(value: number) {
    this._currentPage = value;
  }

  get totalItems(): number {
    return this._totalItems;
  }

  set totalItems(value: number) {
    this._totalItems = value;
  }

  get pageSize(): number {
    return this._pageSize;
  }

  set pageSize(value: number) {
    this._pageSize = value;
  }

  get logs(): Log[] {
    return this._logs;
  }

  set logs(value: Log[]) {
    this._logs = value;
  }

  get markedLogId(): number {
    return this._markedLogId;
  }

  set markedLogId(value: number) {
    this._markedLogId = value;
  }

  constructor(private httpClient: HttpClient, private tokenStorage: TokenStorage) {
    this.dateStart = (Date.now() - MILLISECONDS_PER_DAY); // minus day
    this.dateEnd = (Date.now());
    const url = `${environment.backendUrl}/getTotalItems?source=${this.currentSource}&start=${this.dateStart}&end=${this.dateEnd}`;
    this.httpClient.get<number>(url, this.getOptions()).subscribe(num => this.totalItems = num);
  }

  private getOptions() {
    let HEADERS = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Authorization': `${this.tokenStorage.getToken()}`
    });

    let OPTIONS = {headers: HEADERS};
    return OPTIONS;
  }

  public getTotalItems(): Observable<number> {
    let url: string;
    if (this.currentSource == undefined || this._currentSource === 'not specified') {
      url = `${environment.backendUrl}/getTotalItems?start=${this.dateStart}&end=${this.dateEnd}`;
    } else {
      url = `${environment.backendUrl}/getTotalItems?start=${this.dateStart}&end=${this.dateEnd}&source=${this._currentSource}`;
    }
    return this.httpClient.get<number>(url, this.getOptions());
  }

  public getSources(): Observable<string[]> {
    // return MOCK_SOURCE_LIST;
    const url = `${environment.backendUrl}/sources`;
    return this.httpClient.get<string[]>(url, this.getOptions());
  }

  public createLink(id: number): string {
    let url = `${environment.frontendUrl}/table` +
      `?id=${id}&source=${this.currentSource}&start=${this.dateStart}&end=${this.dateEnd}&page=${this.currentPage}`;
    return url;
  }

  public getLogs() {
    // this.logs = MOCK_DATA;
    console.log(this.currentSource);
    if (this.currentSource == undefined || this.currentSource == 'not specified') {
      this.logs = MOCK_DATA;
    } else {
      let temp_data: Log[] = [];
      for (let i = 0; i < MOCK_DATA.length; i++) {
        if (MOCK_DATA[i].source == this.currentSource) {
          temp_data.push(MOCK_DATA[i]);
        }
      }
      this.logs = temp_data;
    }
    // let url: string;
    // this.getTotalItems().subscribe(num => {
    //   this.totalItems = num;
    //   if (this.currentSource == undefined || this.currentSource == 'not specified') {
    //     url = `${environment.backendUrl}/logs?start=${this.dateStart}&end=${this.dateEnd}&pageNum=${this.currentPage}&pageSize=${this.pageSize}`;
    //   } else {
    //     url = `${environment.backendUrl}/logs?start=${this.dateStart}&end=${this.dateEnd}&source=${this.currentSource}&pageNum=${this.currentPage}&pageSize=${this.pageSize}`;
    //   }
    //   this.httpClient.get<Log[]>(url, this.getOptions()).subscribe(logs => {
    //     this.logs = logs;
    //   });
    // });
  }

  public save(): void {
    let url = '';
    if (this.currentSource == undefined || this.currentSource == 'not specified') {
      url = `${environment.backendUrl}/logs?start=${this.dateStart}&end=${this.dateEnd}&size=${this.totalItems}`;
    } else {
      url = `${environment.backendUrl}/logs?start=${this.dateStart}&end=${this.dateEnd}&source=${this.currentSource}&size=${this.totalItems}`;
    }
    this.httpClient.get(url, {responseType: 'text', headers: {'Authorization': `${this.tokenStorage.getToken()}`}}).subscribe(logs => {
      const file = new File(
        [logs],
        `logs.txt`,
        {type: 'text/plain;charset=utf-8'}
      );
      saveAs(file);
    });
  }
}

const MOCK_SOURCE_LIST: string[] = [
  'Data Receiver 3.5.1',
  'Payment Supporter 1.0.2',
  'Access Defender 4.0'
];


const MOCK_DATA: Log[] = [
  {source: MOCK_SOURCE_LIST[0], id: 1, dateTime: 1576320960201, message: 'it\'s OK'},
  {source: MOCK_SOURCE_LIST[1], id: 2, dateTime: 1500000000002, message: 'it isn\'t OK'},
  {source: MOCK_SOURCE_LIST[2], id: 3, dateTime: 1576320970201, message: 'it\'s OK'},
  {source: MOCK_SOURCE_LIST[0], id: 4, dateTime: 1500000000004, message: 'it isn\'t OK'},
  {source: MOCK_SOURCE_LIST[1], id: 5, dateTime: 1500000000005, message: 'it\'s OK'},
  {source: MOCK_SOURCE_LIST[2], id: 6, dateTime: 1576320780201, message: 'it isn\'t OK'},
  {source: MOCK_SOURCE_LIST[2], id: 7, dateTime: 1576320980201, message: 'it isn\'t OK'},
  {source: MOCK_SOURCE_LIST[1], id: 8, dateTime: 1500000000006, message: 'it isn\'t OK'},
  {source: MOCK_SOURCE_LIST[0], id: 9, dateTime: 1576320980201, message: 'it isn\'t OK'},
  {source: MOCK_SOURCE_LIST[2], id: 10, dateTime: 1576320985032, message: 'it isn\'t OK'},
  {source: MOCK_SOURCE_LIST[1], id: 11, dateTime: 1576420930201, message: 'it isn\'t OK'}
];

function isBigEnough(element, index, array) {
  return (element >= 10);
}
